<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('product',['products'=>$products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
        return view('addproduct');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->hasFile('image')) {
            $filenamewithExt = $request->file('image')->getClientOriginalName();
            $filename = pathinfo($filenamewithExt,PATHINFO_FILENAME);
            $filenameExt = $request->file('image')->getClientOriginalExtension();
            $filenameToStore = $filenamewithExt.'_'.time().'.'.$filenameExt;
            $path = $request->file('image')->storeAs('public/images',$filenameToStore);
        }
        else{
            $filenameToStore = 'noimage.jpg';
        }
        $product = Product::create([
            'productname'=>$request->input('productname'),
            'description'=>$request->input('description'),
            'image'=>$filenameToStore,
            'quantity'=>$request->input('quantity'),
            'category'=>$request->input('category'),
            'price'=>$request->input('price'),
        ]);

        if($product)
        {
            return redirect()->route('products');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if ($request->hasFile('image')) {
            $filenamewithExt = $request->file('image')->getClientOriginalName();
            $filename = pathinfo($filenamewithExt,PATHINFO_FILENAME);
            $filenameExt = $request->file('image')->getClientOriginalExtension();
            $filenameToStore = $filenamewithExt.'_'.time().'.'.$filenameExt;
            $path = $request->file('image')->storeAs('public/images',$filenameToStore);
        }
        else{
            $filenameToStore = 'noimage.jpg';
        }
        $product = Product::find($id);
        Product::where(['id'=>$product->id])->update([
            'productname'=>$request->input('productname'),
            'description'=>$request->input('description'),
            'image'=>$filenameToStore,
            'quantity'=>$request->input('quantity'),
            'category'=>$request->input('category'),
            'price'=>$request->input('price'),
        ]);
        return redirect()->route('products');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {

        $product = Product::find($id);
        $product_id = $id;

        return view('editproduct')
        ->with('product_id', $product_id)
        ->with('product', $product);

        // return redirect()->route('products');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();
        return redirect()->route('products');
    }
}
