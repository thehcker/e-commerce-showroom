@extends('layouts.master')

@section('title')
Welcome
@endsection

@section('content')
@if(count($errors) > 0)
<div class="row">
    <div class="col-md-4 col-md-offset-4" style="color:red;">
        <ul>
            @foreach($errors->all() as $error)
            <li>
                {{$error}}
            </li>
            @endforeach
        </ul>
    </div>
</div>
@endif
<div class="row">
        <div class="col-6">
            <h2>Sign Up</h2>
        <form action="{{ URL('signup') }}" method="post">
        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
            <label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
            <div class="col-sm-10">
            <input type="text" id="staticEmail" name="email" value="{{ Request::old('email')}}">
            </div>
        </div>
        <div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
            <label for="first_name" class="col-sm-2 col-form-label">Your First Name</label>
            <div class="col-sm-10">
            <input type="text" id="first_name" name="first_name" value="{{ Request::old('first_name')}}">
            </div>
        </div>
        <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
            <label for="role" class="col-sm-2 col-form-label">Role</label>
            <div class="col-sm-10">
            <select id="role" name="role">
            <option >customer</option>
            <option >show room owner</option>
            </select>
            </div>
        </div>
        <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
            <label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
            <div class="col-sm-10">
            <input type="password" class="form-control" id="inputPassword" name="password" value="{{ Request::old('password')}}">
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
        <input type="hidden" name="_token" value="{{ Session::token() }}">
        </form>
        </div>
        <div class="col-6">
            <h2>Sign In</h2>
        <form method="post" action="{{ URL('signin')}}">
        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
            <label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
            <div class="col-sm-10">
            <input type="text" id="staticEmail" name="email" value="{{ Request::old('email')}}">
            </div>
        </div>
        <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
            <label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
            <div class="col-sm-10">
            <input type="password" class="form-control" id="inputPassword" name="password" value="{{ Request::old('password')}}">
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
        <input type="hidden" name="_token" value="{{ Session::token() }}">

        </form>
        </div>
</div>
@endsection

