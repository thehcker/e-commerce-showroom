@extends('layouts.master')

@section('title')
Welcome
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div col-md-offset-3" class="col-md-6">
        <form method="POST" action="add_product" enctype="multipart/form-data">
            @csrf
        <label for="productname">Product Name</label>
        <input class="form-control" required type="text" id="productname" name="productname">
        <label for="description">Description</label>
            <textarea class="form-control" type="text" id="description" name="description">
            </textarea>
            <label for="image">Image</label>
            <input class="form-control" type="file" id="image" name="image">
            <label for="quantity">Quantity</label>
            <input class="form-control" type="" id="quantity" name="quantity">
            <label for="price">Price</label>
            <input class="form-control" type="" id="price" name="price">
            <label for="category">Category</label>
            <select class="form-control" type="text" id="category" name="category">
                <option>Toyota</option>
                <option>Mazda</option>
                <option>Mitsubishi</option>
                <option>Mercedes</option>
                <option>Subaru</option>
            </select>

            <button type="submit" btn-btn-primary>Submit</button>

        </form>
        </div>
    </div>
</div>

@endsection