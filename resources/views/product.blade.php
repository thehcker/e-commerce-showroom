@extends('layouts.master')

@section('title')
Welcome
@endsection

@section('content')
<div class="row row-cols-1 row-cols-md-3 g-4">
  <?php
  if($products){
    foreach($products as $product){
      ?>
      <div class="col">
      <div class="card">
        <img src="{{ asset('storage/images/' .$product->image) }}" class="card-img-top" alt="...">
        <div class="card-body">
          <h5 class="card-title"> <?php echo $product->productname; ?></h5>
          <p class="card-text"> <?php echo $product->description; ?></p>
          <p class="card-text"> <?php echo $product->image; ?></p>
          <p class="card-text"> <?php echo $product->quantity; ?></p>
          <p class="card-text"> <?php echo $product->category; ?></p>
          <a href="destroy/<?php echo $product->id; ?>"><button  type="submit" btn btn-danger>Delete</button></a>
          <a href="{{ url('update/'.$product->id) }}"><button  type="submit" btn btn-secondary>Edit</button></a>
        </div>
      </div>
    </div>
      <?php 
      
     
    }
  }
  ?>
  
  
</div>

@endsection