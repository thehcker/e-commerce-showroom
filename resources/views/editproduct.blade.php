@extends('layouts.master')

@section('title')
Welcome
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div col-md-offset-3" class="col-md-6">
        <form method="POST" action="{{ url('edit/'.$product->id) }}" enctype="multipart/form-data">
            @csrf
        <label for="productname">Product Name</label>
        <input class="form-control" required type="text" id="productname" name="productname" value="<?php echo $product->productname ?>">
        <label for="description">Description</label>
            <textarea class="form-control" type="text" id="description" name="description" value="">
            <?php echo $product->description ?>
            </textarea>
            <label for="image">Image</label>
            <input class="form-control" type="file" id="image" name="image" value="<?php echo $product->image ?>">
            <label for="quantity">Quantity</label>
            <input class="form-control" type="" id="quantity" name="quantity" value="<?php echo $product->quantity ?>">
            <label for="price">Price</label>
            <input class="form-control" type="" id="price" name="price" value="{{ $product->price }}">
            <label for="category">Category</label>
            <select class="form-control" type="text" id="category" name="category" value="">
                <option><?php echo $product->category ?></option>
                <option>Mazda</option>
                <option>Mitsubishi</option>
                <option>Mercedes</option>
                <option>Subaru</option>
            </select>

            <button type="submit" btn-btn-primary>Submit</button>

        </form>
       
        </div>
    </div>
</div>

@endsection