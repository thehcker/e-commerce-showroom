<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProductController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['middleware' => ['web']], function(){
    Route::get('/', function () {
        return view('welcome');
    })->name('home');
    Route::get('/products', [ProductController::class, 'index', 'middleware' => 'auth'])->name('products');
    Route::get('/addproduct', [ProductController::class, 'create', 'middleware' => 'auth'])->name('addproduct');
    Route::post('/add_product', [ProductController::class, 'store', 'middleware' => 'auth'])->name('saveproduct');
    Route::post('edit/{id}', [ProductController::class, 'edit', 'middleware' => 'auth'])->name('editproduct');
    Route::get('/update/{id}', [ProductController::class, 'update', 'middleware' => 'auth'])->name('updateproduct');
    Route::get('/destroy/{id}', [ProductController::class, 'destroy', 'middleware' => 'auth'])->name('deleteproduct');
    Route::get('/dashboard', [UserController::class, 'getDashboard', 'middleware' => 'auth'])->name('dashboard');
    Route::post('/signup',[UserController::class, 'postSignUp']);

    Route::post('/signin',[UserController::class, 'postSignIn']);
    Route::get('/logout', [UserController::class, 'getLogout',])->name('logout');

});
